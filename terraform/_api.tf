module "s3_elb_api_logs" {
  source = "terraform-aws-modules/s3-bucket/aws"
  bucket = "threetier-api-logs"
  acl    = "log-delivery-write"
  attach_elb_log_delivery_policy = true
  force_destroy = true
}

resource "aws_route53_record" "route53_api" {
  zone_id = var.domain_zone_id
  name = "api.${var.domain_name}"
  type = "A"
  alias {
    name = module.elb_api.this_elb_dns_name
    zone_id = module.elb_api.this_elb_zone_id
    evaluate_target_health = true
  }
}

module "elb_api" {
  source = "terraform-aws-modules/elb/aws"

  name = "elb-threetier-api"

  subnets = module.vpc.public_subnets
  security_groups = [
    module.vpc.default_security_group_id,
    aws_security_group.allow_http.id
  ]
  internal = false

  listener = [
    {
      instance_port = "80"
      instance_protocol = "http"
      lb_port = "443"
      lb_protocol = "https"
      ssl_certificate_id = module.acm.this_acm_certificate_arn
    },
  ]

  health_check = {
    target              = "HTTP:80/api/status"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  access_logs = {
    bucket = module.s3_elb_api_logs.this_s3_bucket_id
  }

  number_of_instances = module.ec2_instances_api.instance_count
  instances           = module.ec2_instances_api.id
}

module "ec2_instances_api" {
  source = "terraform-aws-modules/ec2-instance/aws"
  name = "3Tier API Instances"
  instance_count = var.api_instance_count
  ami = "ami-0323c3dd2da7fb37d"
  instance_type = "t2.micro"
  key_name = "sshkey"
  vpc_security_group_ids = [
    module.vpc.default_security_group_id,
    aws_security_group.allow_http.id,
    aws_security_group.allow_ssh.id
  ]
  subnet_id = module.vpc.public_subnets[0]

  user_data = <<EOF
    #! /bin/bash
    cat <<EOEXEC | sudo -u ec2-user bash
      sudo amazon-linux-extras enable nginx1.12
      sudo yum -y install nginx git
      # add deploy key
      tee ~/.ssh/id_ed25519 <<EOT
${file(var.git_private_deploy_key_path)}
EOT
      chmod 0600 ~/.ssh/id_ed25519
      # clone repo
      ssh-keyscan ${var.gitlab_base_domain} >> ~/.ssh/known_hosts
      cd ~ && git clone -q ${var.git_repo} threetier
      # install node (via nvm) and pm2
      curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
      . ~/.nvm/nvm.sh
      nvm install 12
      npm install -g pm2
      # copy nginx config files
      cd ~/threetier/terraform/api
      sudo cp nginx.conf /etc/nginx/nginx.conf
      sudo cp api.conf /etc/nginx/conf.d/api.conf
      # setup app
      cd ~/threetier/api
      npm install
      # start api via pm2 and nginx
      echo export PORT=8000 >> ~/.bashrc
      echo export DB=postgres://${module.db.this_db_instance_username}:${module.db.this_db_instance_password}@${module.db.this_db_instance_endpoint}/${module.db.this_db_instance_name} >> ~/.bashrc
      . ~/.bashrc
      pm2 start npm -- start
      sudo systemctl start nginx
      # setup filebeat
      cd ~/threetier/terraform/api
      ELASTICSEARCH_IP=${module.ec2_instance_monitoring.public_ip[0]} sh ./filebeat-setup.sh
EOEXEC
  EOF
}
