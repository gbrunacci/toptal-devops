resource "random_password" "db_password" {
  length = 8
  special = true
  override_special = ":_%@#"
}

module "db" {
  source = "terraform-aws-modules/rds/aws"
  identifier = "threetier-db"
  engine = "postgres"
  engine_version = "11.6"
  name = "threetier"
  username = "threetier"
  password = random_password.db_password.result
  port = 5432
  allocated_storage = 5
  instance_class = "db.t2.micro"
  maintenance_window = "Sun:00:00-Sun:03:00"
  backup_window      = "03:00-06:00"
  backup_retention_period = 30
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]
  subnet_ids = module.vpc.private_subnets
  family = "postgres11"
  major_engine_version = "11"
  final_snapshot_identifier = "threetier-db"
  deletion_protection = false
  vpc_security_group_ids = [module.vpc.default_security_group_id]
}