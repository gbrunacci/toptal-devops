output "web_ip_addresses" {
  value = module.ec2_instances_web.public_ip
}

output "api_ip_addresses" {
  value = module.ec2_instances_api.public_ip
}

output "monitoring_ip_address" {
  value = module.ec2_instance_monitoring.public_ip
}

output "db_threetier_password" {
  value = module.db.this_db_instance_password
}

output "aws_access_key" {
  value = aws_iam_access_key.user_access_key.id
}

output "aws_secret_key" {
  value = aws_iam_access_key.user_access_key.secret
}

output "web_url" {
  value = "web.${var.domain_name}"
}

output "api_url" {
  value = "api.${var.domain_name}"
}

output "cdn_url" {
  value = "cdn.${var.domain_name}"
}

output "grafana_url" {
  value = "monitoring.${var.domain_name}:3000"
}

output "kibana_url" {
  value = "monitoring.${var.domain_name}:5601"
}

output "sample_ssh_to_ec2" {
  value = "ssh -i ./ssh-keys/threetier_rsa ec2-user@INSTANCE_IP"
}