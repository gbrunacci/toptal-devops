module "cdn" {
  source = "git::https://github.com/cloudposse/terraform-aws-cloudfront-s3-cdn.git?ref=0.23.1"
  namespace = "threetier"
  stage = "prod"
  name = "cdn"
  aliases = ["cdn.${var.domain_name}"]
  parent_zone_id = var.domain_zone_id
  acm_certificate_arn = module.acm.this_acm_certificate_arn
  viewer_protocol_policy = "redirect-to-https"
  origin_force_destroy = true
  default_ttl = 86400
  compress = true
  cors_allowed_headers = ["*"]
  cors_allowed_methods = ["GET", "HEAD", "PUT"]
  cors_allowed_origins = ["*.${var.domain_name}"]
  cors_expose_headers = ["ETag", "Cache-Control"]
}

variable "upload_directory" {
  default = "../web/public/"
}

variable "mime_types" {
  default = {
    html  = "text/html"
    css   = "text/css"
    js    = "application/javascript"
    gif   = "image/gif"
  }
}

resource "aws_s3_bucket_object" "upload_files_to_cdn" {
  for_each = fileset("../web/public", "**/*.*")
  bucket = module.cdn.s3_bucket
  key = replace(each.value, var.upload_directory, "")
  source = "${var.upload_directory}${each.value}"
  etag = filemd5("${var.upload_directory}${each.value}")
  content_type = lookup(var.mime_types, split(".", each.value)[length(split(".", each.value)) - 1])
  cache_control = "max-age=31536000"
}