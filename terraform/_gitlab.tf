resource "gitlab_project_variable" "api_server_hosts" {
    project = var.gitlab_project_id
    key = "API_SERVER_HOSTS"
    value = join(" ", module.ec2_instances_api.public_ip)
    environment_scope = "*"
}

resource "gitlab_project_variable" "web_server_hosts" {
    project = var.gitlab_project_id
    key = "WEB_SERVER_HOSTS"
    value = join(" ", module.ec2_instances_web.public_ip)
    environment_scope = "*"
}

resource "gitlab_project_variable" "ssh_private_key" {
    project = var.gitlab_project_id
    key = "SSH_PRIVATE_KEY"
    value = base64encode(file(var.ssh_private_key_path))
    environment_scope = "*"
}

resource "gitlab_project_variable" "git_private_key" {
    project = var.gitlab_project_id
    key = "GIT_PRIVATE_KEY"
    value = base64encode(file(var.git_private_deploy_key_path))
    environment_scope = "*"
}

resource "gitlab_project_variable" "aws_access_key_id" {
    project = var.gitlab_project_id
    key = "AWS_ACCESS_KEY_ID"
    value = aws_iam_access_key.user_access_key.id
    environment_scope = "*"
}

resource "gitlab_project_variable" "aws_access_secret_key" {
    project = var.gitlab_project_id
    key = "AWS_SECRET_ACCESS_KEY"
    value = aws_iam_access_key.user_access_key.secret
    environment_scope = "*"
}

resource "gitlab_project_variable" "aws_default_region" {
    project = var.gitlab_project_id
    key = "AWS_DEFAULT_REGION"
    value = "us-east-1"
    environment_scope = "*"
}

resource "gitlab_project_variable" "cdn_s3_bucket" {
    project = var.gitlab_project_id
    key = "CDN_S3_BUCKET"
    value = module.cdn.s3_bucket
    environment_scope = "*"
}
