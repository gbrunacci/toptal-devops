resource "aws_route53_record" "route53_monitoring" {
  zone_id = var.domain_zone_id
  name = "monitoring.${var.domain_name}"
  type = "A"
  ttl = "300"
  records = module.ec2_instance_monitoring.public_ip
}

module "s3_functionbeat" {
  source = "terraform-aws-modules/s3-bucket/aws"
  bucket = "threetier-functionbeat"
  acl    = "log-delivery-write"
  force_destroy = true
}

module "ec2_instance_monitoring" {
  source = "terraform-aws-modules/ec2-instance/aws"
  name = "3Tier Monitoring Instance"
  instance_count = 1
  ami = "ami-0323c3dd2da7fb37d"
  instance_type = "t2.medium"
  key_name = "sshkey"
  vpc_security_group_ids = [
    module.vpc.default_security_group_id,
    aws_security_group.allow_http.id,
    aws_security_group.allow_ssh.id,
    aws_security_group.allow_monitoring.id
  ]
  subnet_id = module.vpc.public_subnets[0]
  user_data = <<EOF
    #! /bin/bash
    cat <<EOEXEC | sudo -u ec2-user bash
      sudo amazon-linux-extras enable docker
      sudo yum -y install docker git
      # configure docker
      sudo systemctl start docker
      # install grafana
      sudo docker run -d --name grafana -p 3000:3000 grafana/grafana
      # install ELK
      sudo sysctl -w vm.max_map_count=262144
      sudo docker run -d -p 5601:5601 -p 9200:9200 -p 5044:5044 --ulimit nofile=1024:65535 -it --name elk sebp/elk
      # clone repo to setup grafana and functionbeat
      tee ~/.ssh/id_ed25519 <<EOT
${file(var.git_private_deploy_key_path)}
EOT
      chmod 0600 ~/.ssh/id_ed25519
      ssh-keyscan ${var.gitlab_base_domain} >> ~/.ssh/known_hosts
      cd ~ && git clone -q ${var.git_repo} threetier
      cd ~/threetier/terraform/monitoring
      # Functionbeat for RDS Monitoring
      AWS_ACCESS_KEY=${aws_iam_access_key.user_access_key.id} \
        AWS_SECRET_KEY=${aws_iam_access_key.user_access_key.secret} \
        FUNCTIONBEAT_BUCKET=${module.s3_functionbeat.this_s3_bucket_id} \
        bash ./functionbeat-setup.sh
      # Grafana datasource and dashboard
      AWS_ACCESS_KEY=${aws_iam_access_key.user_access_key.id} \
        AWS_SECRET_KEY=${aws_iam_access_key.user_access_key.secret} \
        bash ./grafana-setup.sh
EOEXEC
  EOF
}

# FIXME: remote-exec it not ending after successful execution
#
# resource "null_resource" "functionbeat_uninstall" {
#   triggers = {
#     ssh_key = var.ssh_private_key_path
#     ssh_host = module.ec2_instance_monitoring.public_ip[0]
#   }

#   connection {
#     type = "ssh"
#     user = "ec2-user"
#     private_key = file(self.triggers.ssh_key)
#     host = self.triggers.ssh_host
#   }

#   provisioner "remote-exec" {
#     when = destroy
#     inline = [
#       "cd ~/threetier/terraform/monitoring",
#       "bash ./functionbeat-remove.sh",
#       "sleep 30 ; exit 0" # force exit and close connection
#     ]
#   }
# }