provider "gitlab" {
    token = var.gitlab_token
    base_url = "https://${var.gitlab_base_domain}"
}

provider "aws" {
  profile = var.aws_profile
  region = "us-east-1"
}

resource "aws_key_pair" "sshkey" {
  key_name = "sshkey"
  public_key = file(var.ssh_public_key_path)
}

resource "aws_iam_user" "user" {
  name = "threetier"
  force_destroy = true
}

resource "aws_iam_user_policy" "user_policy" {
  name = "threetier-policy"
  user = aws_iam_user.user.name
  policy = <<EOF
${file("iam/policy.json")}
  EOF
}

resource "aws_iam_access_key" "user_access_key" {
  user = aws_iam_user.user.name
}