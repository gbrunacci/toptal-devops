#!/bin/bash
set +x
curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.6.2-x86_64.rpm
sudo rpm -vi filebeat-7.6.2-x86_64.rpm
cp filebeat.base.yml filebeat.yml
sed -i "s/ELASTICSEARCH_IP/$ELASTICSEARCH_IP/g" filebeat.yml
sudo mv filebeat.yml /etc/filebeat/filebeat.yml
sudo chmod go-w /etc/filebeat/filebeat.yml
sudo chown root.root /etc/filebeat/filebeat.yml
sudo systemctl restart filebeat
sudo systemctl enable filebeat
