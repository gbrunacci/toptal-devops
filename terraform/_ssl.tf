module "acm" {
  source = "terraform-aws-modules/acm/aws"
  domain_name = var.domain_name
  zone_id = var.domain_zone_id
  subject_alternative_names = [
    "*.${var.domain_name}"
  ]
}