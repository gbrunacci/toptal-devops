#!/bin/bash
set +x
curl -v http://admin:admin@127.0.0.1:3000/api/datasources \
        -X POST \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        --data-binary @- <<EOJSON
{
  "name": "CloudWatch",
  "type": "cloudwatch",
  "access": "proxy",
  "url": "http://monitoring.us-east-1.amazonaws.com",
  "jsonData": {
    "authType": "keys",
    "defaultRegion": "us-east-1"
  },
  "secureJsonData": {
    "accessKey": "${AWS_ACCESS_KEY}",
    "secretKey": "${AWS_SECRET_KEY}"
  },
  "readOnly": false
}
EOJSON
      curl -v http://admin:admin@127.0.0.1:3000/api/dashboards/db \
        -X POST \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        --data-binary @grafana-dashboard.json