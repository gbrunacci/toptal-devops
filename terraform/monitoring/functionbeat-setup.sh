#!/bin/bash
set +x
# get self public ip
ELASTICSEARCH_IP=$(curl -s https://api.ipify.org)
# inject aws credentials
mkdir -p ~/.aws
tee ~/.aws/credentials <<EOT
[default]
aws_access_key_id = $AWS_ACCESS_KEY
aws_secret_access_key = $AWS_SECRET_KEY
EOT
tee ~/.aws/config <<EOT
[default]
region = us-east-1
EOT
# setup functionbeat
curl -L -O https://artifacts.elastic.co/downloads/beats/functionbeat/functionbeat-7.6.2-linux-x86_64.tar.gz
tar xzf functionbeat-7.6.2-linux-x86_64.tar.gz
cd functionbeat-7.6.2-linux-x86_64
cp ../functionbeat.base.yml functionbeat.yml
sed -i "s/ELASTICSEARCH_IP/$ELASTICSEARCH_IP/g" functionbeat.yml
sed -i "s/FUNCTIONBEAT_BUCKET/$FUNCTIONBEAT_BUCKET/g" functionbeat.yml
# deploy functionbeat
./functionbeat -v -e -d "*" deploy cloudwatch