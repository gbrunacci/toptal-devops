/*
 * Name of AWS Profile setup (usually "default")
 */
variable "aws_profile" {
  type = string
  default = "default"
}

/*
 * Relative path to SSH Public Key
 */
variable "ssh_public_key_path" {
  type = string
  default = "ssh-keys/threetier_rsa.pub"
}

/*
 * Relative path to SSH Private Key
 */
variable "ssh_private_key_path" {
  type = string
  default = "ssh-keys/threetier_rsa"
}

/*
 * Name of Route53 Hosted Zone where the app will be hosted.
 */
variable "domain_name" {
  type = string
  default = "gbtestsite.xyz"
}

/*
 * Hosted Zone ID for domain specified in domain_name
 */
variable "domain_zone_id" {
  type = string
  default = "Z057635523OCXW9CRCFG4"
}

/*
 * Number of EC2 Instances that will serve as API nodes
 */
variable "api_instance_count" {
  type = number
  default = 1
}

/*
 * Number of EC2 Instances that will serve as WEB nodes
 */
variable "web_instance_count" {
  type = number
  default = 1
}

/*
 * Relative path to Git deploy Private Key
 */
variable "git_private_deploy_key_path" {
  type = string
  default = "ssh-keys/id_ed25519"
}

/*
 * Relative path to Git deploy Public Key
 */
variable "git_public_deploy_key_path" {
  type = string
  default = "ssh-keys/id_ed25519.pub"
}

/*
 * GitLab Personal Access Token.
 * Permissions Required: "api"
 * Docs: https://gitlab.com/profile/personal_access_tokens
 */
variable "gitlab_token" {
  type = string
  default = "tsc9kR-UN-Cw4r8GU3xs"
}

/*
 * GitLab Project ID. Can be found in repository home page.
 */
variable "gitlab_project_id" {
  type = number
  default = "18280495"
}

/*
 * GitLab Base URL. Customize for self-hosted instances.
 */
variable "gitlab_base_domain" {
  type = string
  default = "gitlab.com"
}

/*
 * Full Repo Path.
 */
variable "git_repo" {
  type = string
  default = "git@gitlab.com:gbrunacci/toptal-devops.git"
}