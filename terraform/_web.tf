module "s3_elb_web_logs" {
  source = "terraform-aws-modules/s3-bucket/aws"
  bucket = "threetier-web-logs"
  acl    = "log-delivery-write"
  attach_elb_log_delivery_policy = true
  force_destroy = true
}

resource "aws_route53_record" "route53_web" {
  zone_id = var.domain_zone_id
  name = "web.${var.domain_name}"
  type = "A"
  alias {
    name = module.elb_web.this_elb_dns_name
    zone_id = module.elb_web.this_elb_zone_id
    evaluate_target_health = true
  }
}

module "elb_web" {
  source = "terraform-aws-modules/elb/aws"

  name = "elb-threetier-web"

  subnets = module.vpc.public_subnets
  security_groups = [
    module.vpc.default_security_group_id,
    aws_security_group.allow_http.id
  ]
  internal = false

  listener = [
    {
      instance_port = "80"
      instance_protocol = "http"
      lb_port = "443"
      lb_protocol = "https"
      ssl_certificate_id = module.acm.this_acm_certificate_arn
    },
  ]

  health_check = {
    target              = "HTTP:80/"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  access_logs = {
    bucket = module.s3_elb_web_logs.this_s3_bucket_id
  }

  number_of_instances = module.ec2_instances_web.instance_count
  instances           = module.ec2_instances_web.id
}

module "ec2_instances_web" {
  source = "terraform-aws-modules/ec2-instance/aws"
  name = "3Tier Web Instances"
  instance_count = var.web_instance_count
  ami = "ami-0323c3dd2da7fb37d"
  instance_type = "t2.micro"
  key_name = "sshkey"
  vpc_security_group_ids = [
    module.vpc.default_security_group_id,
    aws_security_group.allow_http.id,
    aws_security_group.allow_ssh.id
  ]
  subnet_id = module.vpc.public_subnets[0]

  user_data = <<EOF
    #! /bin/bash
    cat <<EOEXEC | sudo -u ec2-user bash
      sudo amazon-linux-extras enable nginx1.12
      sudo yum -y install nginx git
      # add deploy key
      tee ~/.ssh/id_ed25519 <<EOT
${file(var.git_private_deploy_key_path)}
EOT
      chmod 0600 ~/.ssh/id_ed25519
      # clone repo
      ssh-keyscan ${var.gitlab_base_domain} >> ~/.ssh/known_hosts
      cd ~ && git clone -q ${var.git_repo} threetier
      # install node (via nvm) and pm2
      curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
      . ~/.nvm/nvm.sh
      nvm install 12
      npm install -g pm2
      # copy nginx config files
      cd ~/threetier/terraform/web
      sudo cp nginx.conf /etc/nginx/nginx.conf
      sudo cp web.conf /etc/nginx/conf.d/web.conf
      # setup app
      cd ~/threetier/web
      npm install
      # start web via pm2 and nginx
      echo export PORT=3000 >> ~/.bashrc
      echo export API_HOST=https://api.gbtestsite.xyz/ >> ~/.bashrc
      . ~/.bashrc
      pm2 start npm -- start
      sudo systemctl start nginx
      # setup filebeat
      cd ~/threetier/terraform/web
      ELASTICSEARCH_IP=${module.ec2_instance_monitoring.public_ip[0]} sh ./filebeat-setup.sh
EOEXEC
  EOF
}
