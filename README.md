# Gabriel Brunacci > TopTal DevOps Screening Process

The following document explains the architecture, tools, and usage of a scalable CI solution for a simple Three Tier Node App, where:

- Infrastructure & several CI variables are managed with Terraform.
- Infrastructure and hosted in AWS.
- CI/CD on push on master branch is being managed by GitLab CI.

## Architecture

![General App Layout](.docs-assets/web-infrastructure.png "General App Layout")

## Terraform connected providers

![Terraform Providers](.docs-assets/terraform-providers.png "Terraform Providers")

## Setup

Clone this repo.

```
git clone git@gitlab.com:gbrunacci/toptal-devops.git
```

### AWS CLI

If you don't have AWS CLI setup yet. On MacOS, via Homebrew, run:

```
brew install awscli
```

Make sure you have the AWS profile profile to where you want to deploy the app set. If you don't, run.

```
aws configure
```

### Deploy Keys

We require two set of deploy keys.

- One RSA Key to SSH into the EC2 Instances.
- One ED25519 Key to clone and pull changes from the code repository.

Place both pairs of private and public key in `terraform/ssh-keys`.

It is recommended to generate brand new keys.

### GitLab Repository Settings

Also, pull from this repo.

### Have a domain setup under Route53

In order to generate valid SSL Certificates and host app under nice names. This project was setup to be served under a named domain.

It is required to have a domain (Hosted Zone) setup under Route53.

### Setup Terraform

Setup settings in `terrform/variables.tf`.

Download modules by running.

```
terraform init
```

## Deploy

```
cd terraform
terraform deploy
```

### Post-deploy Defaults

- Web URL: https://web.gbtestsite.xyz
- API URL: https://api.gbtestsite.xyz
- CDN URL: https://cdn.gbtestsite.xyz
- Grafana URL: http://monitoring.gbtestsite.xyz:3000 (user: admin/admin)
- Kibana URL: http://monitoring.gbtestsite.xyz:5601

### Gitlab CI

The following CI Variables are set after applying the infrastructure.

- `API_SERVER_HOSTS`: List of EC2 instances where API should update
- `WEB_SERVER_HOSTS`: List of EC2 instances where WEB should update
- `SSH_PRIVATE_KEY`: Private key used to log into both WEB and API EC2 instances.
- `GIT_PRIVATE_KEY`: Private key to clone the repo
- `AWS_ACCESS_KEY_ID`: Terraform generated AWS Key to Upload files to S3 via GitLab CI
- `AWS_SECRET_ACCESS_KEY`: Terraform generated AWS Secret Key to Upload files to S3 via GitLab CI.
- `AWS_DEFAULT_REGION`: Default AWS Region
- `CDN_S3_BUCKET`: Bucket where to uplaod CDN assets.

### Caveat: Not seeing RDS logs in Kibana

RDS Logs are being reported via FunctionBeat, but Kibana by default shows only logs from `filebeat-*` indexes.

Under Logs > Settings > Replace index filter from `fiilebeat-*` to `*` to see logs from all sources.

## See IPs, URLs, generated passwords

```
cd terraform
terraform show
```

## Scale number of Instances

Under `terraform/variables.tf` you can setup the following vars:

- api_instance_count = number of EC2 Instances serving API Layer
- web_instance_count = number of EC2 Instances serving WEB Layer

## Teardown infrastructure

As of 4/27 -- we first need to teardown functionbeat manually (see `terraform/_monitoring.tf` for details)

```
cd terraform
ssh -i ssh-keys/threetier_rsa ec2-user@MONITORING_INSTANCE_IP "cd ~/threetier/terraform/monitoring && bash ./functionbeat-remove.sh"
```

And then destroy the rest via Terraform.

```
terraform destroy
```

## GitLab CI Pipeline

GitLab CI will.

- On Push: Clone and try to install npm packages on both web and api to make sure nothing obvious breaks

- On Push/merge to `master` branch: clone, install, and deploy both web and api.

![GitLab Pipeline](.docs-assets/gitlab-pipeline.png "GitLab Pipeline")
